#============================================================================
# pkgconfig_subst.mk

# create the pkg-config config file.
PKGCONFIG_SUBST_MK =

# Preloaded Snippets: create_am_macros.mk
DOSUBST_MK +=
CREATE_AM_MACROS_MK +=

EXTRA_DIST              += $(PACKAGE_NAME).pc.in
CLEANFILES		+= $(PACKAGE_NAME).pc

pkgconfigdatadir	= $(libdir)/pkgconfig
pkgconfigdata_DATA	= $(PACKAGE_NAME).pc

# this snippet performs variable interpolation into the pkg-config
# meta-data file using dosubst
$(PACKAGE_NAME).pc: $(PACKAGE_NAME).pc.in
	p="$@"; f=$(strip_dir) \
	$(dosubst) $(srcdir)/$$f.in > $$f || { rm $$f ; false ; }
