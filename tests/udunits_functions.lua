local common = require( 'common' )

local ut = require( 'udunits2' )

local time_t = { year   = 2011,
		 month  = 9,
		 day    = 12,
		 hour   = 13,
		 minute = 53,
		 second = 08 }

local time_double = 337528388

describe( "udunits function",
	 function ()

	    setup( common.setup )

	    it( "trim",
	       function ()

		  local trimmed = ut.trim( '\nbxxxx ', ut.ASCII )

		  assert.is_equal( 'bxxxx', trimmed )

	       end
	    )

	    it( "encode",
	       function ()

		  local time = ut.encode_time(
					      time_t.year,
					      time_t.month,
					      time_t.day,
					      time_t.hour,
					      time_t.minute,
					      time_t.second
					   );

		  local date = ut.encode_date( time_t.year, time_t.month, time_t.day )
		  local clock = ut.encode_clock( time_t.hour, time_t.minute, time_t.second )

		  assert.is_equal( time_double, time )
		  assert.is_equal( date+clock, time )

	       end
	    )

	 it( 'decode_time',
	    function ()
	       local t = {}

	       t.year, t.month, t.day, t.hour, t.minute, t.second
		  = ut.decode_time( time_double )

	       assert.are.same( time_t, t )
	    end
	 )


	 it( "status",
	    function ()
	       ut.set_status( ut.BAD_ARG )
	       assert.is_equal( ut.BAD_ARG, ut.get_status( ) )
	    end
	 )

	 describe( "enums",

		  function ()

		     it( "status",
			function ()
			   local enum = {
			      [0] = 'SUCCESS',
			      'BAD_ARG',
			      'EXISTS',
			      'NO_UNIT',
			      'OS',
			      'NOT_SAME_SYSTEM',
			      'MEANINGLESS',
			      'NO_SECOND',
			      'VISIT_ERROR',
			      'CANT_FORMAT',
			      'SYNTAX',
			      'UNKNOWN',
			      'OPEN_ARG',
			      'OPEN_ENV',
			      'OPEN_DEFAULT',
			      'PARSE'
			   }

			   -- need to do this one at a time as ut has
			   -- more elements in it than enum does

			   for v,k in pairs( enum ) do
			      assert.is_equal( v, ut[k], k )
			   end

			end
		     )

		     it( "encoding",
			function ()

			   local enum = {
			      ASCII = 0,
			      ISO_8859_1 = 1,
			      LATIN1 = 1,
			      UTF8 = 2
			   }

			   for k,v in pairs( enum ) do
			      assert.is_equal( v, ut[k], k )
			   end

			end
		     )

		  end
	       )

	 it( "error messages lua",
		  function ()

		     local stack = {}

		     local func = function ( message )
				     stack[#stack+1] = message
				  end

		     local handler = ut.set_error_message_handler( func )

		     assert.are.equals( common.error_message_handler, handler );

		     ut.handle_error_message( "message 1")
		     ut.handle_error_message( "message 2")

		     assert.is_equal( "message 1", stack[1] );
		     assert.is_equal( "message 2", stack[2] );

		     -- reset original handler
		     local ok, msg = pcall( ut.set_error_message_handler, handler )
		     assert.is_true( ok, msg )

		  end
	       )


	 it ( "error messagesC",
	     function ()

		local ut_test = require( 'udunits2_test' )

		ut_test.set_error_message_handler( )

		local last = ut.set_error_message_handler( 'stderr' )

		assert.is_true( ut.isa( last, 'error_message_handler' ) )

	     end
	  )


      end
      )

