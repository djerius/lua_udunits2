local common = require( 'common' )
local ut = require( 'udunits2' )
local uts = require( 'udunits2.system' )

describe( "system functions", function ()

  setup( common.setup )

  it( "read_xml", function ()
		     assert.has_no.errors( function () common.read_xml() end )
  end)

  it( "new", function ()
		assert.has_no.errors( function () uts.new() end )
  end)

end)
