local common = require( 'common' )
io.output( io.stderr )

local ut = require( 'udunits2' )
local uts = require( 'udunits2.system' )

describe( 'default system',
	 function ()

	    setup( common.setup )

	    local system

	    it( "load",
	       function ()
	    	  assert.has_no.errors( function () system = read_xml() end )
	       end
	    )

	    it( "is a system",
	       function ()
	    	  assert.is_true( ut.isa( system, 'system' ) )
	       end
	    )

	    it( "has a dimensionless unit one",
	       function ()
	    	  assert.are_not.equals( nil, system:get_dimensionless_unit_one() )
	       end
	    )

	    it( "has the radian unit",
	       function ()
	    	  assert.are_not.equals( nil, system:get_unit_by_name( 'radian' ) )
	       end
	    )

	    it( "has the degrees unit",
	       function ()
	    	  assert.are_not.equals( nil, system:get_unit_by_name( 'degrees' ) )
	       end
	    )

	    it( "has the ' unit",
	       function ()
	    	  assert.are_not.equals( nil, system:get_unit_by_symbol( "'" ) )
	       end
	    )

	    it( "can parse a unit",
	       function ()
		  local unit
		  assert.has_no.errors( function () unit = system:parse( "20 degrees_C" ) end )
		  assert.is_true( ut.isa( unit, 'unit' ) )
	       end
	    )

	 end
      )

describe( 'empty system',
	 function ()

	    setup( common.setup )

	    local system

	    it( "can create",
	       function ()
	    	  assert.has_no.errors( function () system = uts.new() end )
	       end
	    )

	    it( "should have the dimensionless unit",
	       function ()
	    	  assert.are_not.equals( nil, system:get_dimensionless_unit_one() )
	       end
	    )
	    it( "should be not have the radian unit",
	       function ()
	    	  assert.are.equals( nil, system:get_unit_by_name( 'radian' ) )
	       end
	    )

	 end
      )

describe( 'units',
	 function ()

	    setup( common.setup )

	    local system

	    it( "load system",
	       function ()
	    	  assert.has_no.errors( function () system = read_xml() end )
	       end
	    )

	    describe( "new base",
		     function()
			local unit
			it( "is created",
			   function()
			      assert.has_no.errors( function () unit = system:new_base_unit() end )
			   end
			)
			it( "is a unit",
			   function ()
			      assert.is_true( ut.isa( unit, 'unit' ) )
			   end
			)
		     end
		  )

	    describe( "new dimesionless unit",
		     function()

			local unit
			it( "is created",
			   function()
			      assert.has_no_errors( function () unit = system:new_dimensionless_unit() end )
			   end
			)


			it( "is a unit",
			   function ()
			      assert.is_true( ut.isa( unit, 'unit' ) )
			   end
			)

		     end
		  )

	    describe( "unmap name to unit",
		     function ()

			it( "has degree unit",
			   function()
			      assert.is_not.equal( nil, system:get_unit_by_name( "degree" ) )
			   end
			)

			it( "is unmapped",
			   function()
			      assert.is_true( system:unmap_name_to_unit( "degree", ut.ASCII ) )
			      assert.is_equal( nil, system:get_unit_by_name( "degree" ) )
			   end
			)

		     end
		  )


	    describe( "unmap symbol to unit",
		     function ()

			it( "has symbol '",
			   function ()
			      assert.is_not.equal( nil, system:get_unit_by_symbol( "'" ) )
			   end
			)

			it( "is unmapped",
			   function ()
			      assert.is_true( system:unmap_symbol_to_unit( "'", ut.ASCII ) )
			      assert.is_equal( nil, system:get_unit_by_symbol( "'" ) )
			   end
			)

		     end
		  )

	 end
      )


