/* --8<--8<--8<--8<--
 *
 * Copyright (C) 2011 Smithsonian Astrophysical Observatory
 *
 * This file is part of lua_udunits2
 *
 * lua_udunits2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -->8-->8-->8-->8-- */


/* for vsnprintf */
#ifndef _ISOC99_SOURCE
#define _ISOC99_SOURCE
#define LUA_UDUNITS_C99
#endif

#include <stdio.h>
#include <stdarg.h>

#ifdef LUA_UDUNITS_C99
#undef _ISOC99_SOURCE
#endif

#include <errno.h>

#include <string.h>
#include <stdlib.h>

#include "lua_udunits2.h"

#define check_optencoding(L,n,d) luaL_opt(L,check_encoding,n,d)


/* forward declarations to keep gcc happy */
int luaopen_udunits2_test( lua_State *L );


static int
handle_error_message( const char *fmt, va_list ap ) {

    return strlen(fmt);
}

/* adds a custom error message handler to test that the next
   call to udunits2.set_error_message_handler will return a properly
   meta-tabled userdata */

static int
set_error_message_handler( lua_State *L ) {

    ut_set_error_message_handler(handle_error_message);

    return 1;
}


/* ----------------------------------------------------------------------------------

   More support routines

 */

static void
register_functions( lua_State* L, luaL_Reg* list ) {

    /* populate the table */
    for( ; list->name ; list++ ) {
	lua_pushcfunction(L, list->func );
	lua_setfield( L, -2, list->name );
    }

}


#define add(name) {#name, name }

static luaL_Reg
 udunits2_test_functions[] = {

    add( set_error_message_handler ),
    { NULL, NULL }
};





int luaopen_udunits2_test( lua_State *L ) {

    lua_newtable( L );
    register_functions( L, udunits2_test_functions );

    return 1;
}
