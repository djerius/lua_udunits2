string = require( 'string' )
ut = require( 'udunits2' )
uts = require( 'udunits2.system' )

function error_message_handler (msg)
   error(msg)
end

function read_xml()

   local error_msg

   local handler = ut.set_error_message_handler(
		function (msg) error_msg = msg end
   )

   local system = uts.read_xml( )

   if not string.find( error_msg, 'overrides' ) then
      error( error_msg )
   end

   ut.set_error_message_handler( handler )

   return system
end


function setup( )

   ut.set_error_message_handler( error_message_handler )

end

return { setup = setup,
	 read_xml = read_xml,
	 error_message_handler = error_message_handler
      }
