local common = require( 'common' )
local ut = require( 'udunits2' )
local uts = require( 'udunits2.system' )

io.output( io.stderr )

describe( 'example', function ()

			setup( common.setup )

   local system = common.read_xml()

   it ( "test conversion", function ()

   local micron
   assert.has_no_error(
		       function()
			  micron = system:get_unit_by_name( 'micron' )
		       end )

   local unit
   assert.has_no_error( function()
			   unit = system:parse( '1e-6 meter' )
			end )

   assert.are.equal( 1, unit:get_converter( micron ):convert(1) )

   end)

end)
