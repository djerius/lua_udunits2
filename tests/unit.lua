local common = require( 'common' )
io.output( io.stderr )


local ut = require( 'udunits2' )
local uts = require( 'udunits2.system' )
local utu = require( 'udunits2.unit' )

describe( "unit",

	 function ()

	    setup( common.setup )

	    local system;
	    it( "create system",
	       function ()
		  assert.has_no.error( function () system = assert( uts.new() ) end )
	       end
	    )

	    local unit1
	    local unit2
	    it( "create and map units",
	       function ()
		  assert.no_errors( function () unit1 = system:new_base_unit() end )
		  assert.not_equals( nil, unit1 );
		  assert.is_true( unit1:map_name_to_unit( 'foo', ut.ASCII ) )

		  assert.no_errors( function () unit2 = system:parse( "20 foo" ) end )
		  assert.not_equals( nil, unit2 );
	       end
	    )

	    describe( "same_system",
	       function ()

		  it( "function",
		     function ()
			assert.is_true( utu.same_system( unit1, unit2 ) )
		     end
		  )
		  it( "method",
		     function ()
			assert.is_true( unit1:same_system( unit2 ) )
		     end
		  )
	       end
	    )

	    describe( "compare",
	       function ()

		  it( "function",
		     function()
			assert.is_true( utu.compare( unit1, unit2 ) < 1 )
		     end
		  )

		  it( "method",
		     function()
			assert.is_true( unit1:compare( unit2 ) < 1 )
		     end
		  )

	       end

	    )

	    describe( "are_convertible",
	       function ()

		  it( "function",
		     function()
			assert.is_true( utu.are_convertible( unit1, unit2 ) )
		     end
		  )

		  it( "method",
		     function()
			assert.is_true( unit1:are_convertible( unit2 ) )
		     end
		  )

	       end

	    )



	    describe( "multiply and divide",
	       function ()

		  local one
		  local two
		  local four

		  it( "create units",
		     function ()
			one = system:get_dimensionless_unit_one()
			assert.not_equals( nil, one )

			two = system:parse( "2" )
			assert.not_equals( nil, two )

			four = system:parse( "4" )
			assert.not_equals( nil, four )

		     end
		  )

		  describe( "multiply",
			   function ()
			      it( "function",
				 function()
				    assert.is_true( one:compare( utu.multiply( two, one ) ) < 1  )
				 end
			      )

			      it( "method",
				 function()
				    assert.is_true( one:compare( two:multiply( one ) ) < 1 )
				 end
			      )

			   end
			)

		  describe( "divide",
			   function ()
			      it( "function",
				 function()
				    assert.is_true( two:compare( utu.divide( four, two) ) == 0  )
				 end
			      )

			      it( "method",
				 function()
				    assert.is_true( two:compare( four:divide( two) ) == 0  )
				 end
			      )

			   end
			)



	       end
	    )


	    it( "scale",
		     function ()

			local meter = system:new_base_unit();
			assert.not_equals( nil, meter )
			local kilometer = meter:scale( 1000 );

			assert.is_true( meter:compare( kilometer ) < 1 )

			assert.is_true( meter:map_name_to_unit( 'm', ut.ASCII ) )

			local unit = system:parse( '1000 m' );

			assert.is_true(  unit:compare( kilometer )  == 0);

		     end
		  )

	    it( "offset",
	       function ()

		  local base = system:new_base_unit()
		  local offset = base:offset( -33 )

		  base:map_name_to_unit( 'base', ut.ASCII )

		  local unit = system:parse( '33 base' )

		  local converter = base:get_converter( offset )

		  assert.is_true( ut.isa( converter, 'converter' ) )

		  local value = converter:convert( 0 )

		  assert.is_true(  unit:compare( offset ) == 1 );
	       end
	    )

	 end
      )
