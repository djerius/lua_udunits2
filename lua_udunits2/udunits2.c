/* --8<--8<--8<--8<--
 *
 * Copyright (C) 2011 Smithsonian Astrophysical Observatory
 *
 * This file is part of lua_udunits2
 *
 * lua_udunits2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -->8-->8-->8-->8-- */


/* for vsnprintf */
#ifndef _ISOC99_SOURCE
#define _ISOC99_SOURCE
#define LUA_UDUNITS_C99
#endif

#include <stdio.h>
#include <stdarg.h>

#ifdef LUA_UDUNITS_C99
#undef _ISOC99_SOURCE
#endif

#include <errno.h>

#include <string.h>
#include <stdlib.h>

#include "lua_udunits2.h"

#define check_optencoding(L,n,d) luaL_opt(L,check_encoding,n,d)


/* forward declarations to keep gcc happy */
int luaopen_udunits2( lua_State *L );
int luaopen_udunits2_system( lua_State *L );
int luaopen_udunits2_unit( lua_State *L );
int luaopen_udunits2_converter( lua_State *L );




/* ***********************************************************************

   Support Routines (Not Exposed)

   *********************************************************************** */


/* -----------------------------------------------------------------------
   typedef int (*ut_error_message_handler) (const char *fmt, va_list args)
   ----------------------------------------------------------------------- */

/* udunit2's error message mechanism doesn't allow passing extra data
   along to the message handler. This horrible kludge allows the
   message handler access to the lua_State object.  Since udunits2
   isn't thread safe anyway (ut_status() returns a global status), I
   don't feel quite as bad as I might about this.
 */

static lua_State *Ls;

/* each Lua wrapper calls this to make sure the stored Lua state is up-to-date
   for that call */
#define set_lua_State(L) Ls = L


/* If the caller specifies a Lua function as the error message handler,
   it gets stored in the Lua registry; these routines manage that
*/

/* NOT EXPOSED */
static void
retrieve_message_handler( lua_State *L ) {

    lua_getfield( L, LUA_REGISTRYINDEX, "udunits2" );
    lua_getfield( L, -1, "message_handler" );
    lua_remove( L, -2 );
}

/* NOT EXPOSED */
/* message handler is on the stack */
static void
store_message_handler( lua_State *L ) {

    lua_getfield( L, LUA_REGISTRYINDEX, "udunits2" );
    lua_pushvalue( L, -2 );
    lua_setfield( L, -2, "message_handler" );
    lua_pop( L, 2 );
}

/* NOT EXPOSED */
static int
lua_message_handler(const char* fmt, va_list args)
{
    char error_message[1024];
    int  size;

    size = vsnprintf( error_message, sizeof(error_message), fmt, args);

    if ( size >= sizeof(error_message))
	size = sizeof(error_message) - 1;

    /* get Lua error message function from registry */
    retrieve_message_handler( Ls );

    lua_pushlstring( Ls, error_message, size );
    lua_call( Ls, 1, 0 );

    return size;
}


/* NOT EXPOSED */
static
ut_encoding check_encoding( lua_State *L, int narg) {

    int encoding = luaL_checkinteger( L, narg );

    if ( encoding < UT_ASCII || encoding > UT_UTF8 ) {
	const char *msg = lua_pushfstring(L, "unknown encoding value '%d'", encoding );
	return luaL_argerror(L, narg, msg);
    }

    return encoding;
}



/* NOT EXPOSED */
#define gc_clean( elem, free )			\
static int \
elem##_gc( lua_State *L ) { \
\
    LuaUTContainer *c = luaut_tocontainer( L, 1 ); \
\
    if ( c->manage && c->p.elem ) { \
	free( c->p.elem ); \
	c->p.elem = NULL; \
    } \
\
    return 0; \
}

gc_clean( system, ut_free_system )
gc_clean( unit, ut_free )
gc_clean( converter, cv_free )


/* NOT EXPOSED */
static int
push_error_status( lua_State *L, int nil ) {

    int serrno = errno;

    if ( nil )
	lua_pushnil( L );
    else
	lua_pushboolean( L, 0 );

    lua_pushinteger( L, ut_get_status() );
    lua_pushinteger( L, serrno );
    return 3;

}

/* NOT EXPOSED */
static int
push_status( lua_State *L, ut_status status ) {

    if ( status != UT_SUCCESS )
	return push_error_status( L, 0 );

    lua_pushboolean( L, 1 );
    return 1;
}

/* NOT EXPOSED */
static int
push_system ( lua_State *L, ut_system* system ) {

    if ( ! system )
	return push_error_status( L, 1 );

    luaut_push_udata_system( L, system, 1 );
    return 1;
}

/* NOT EXPOSED */
static int
push_unit ( lua_State *L, ut_unit* unit, int nil ) {

    if ( ! unit )
	return push_error_status( L, 1 );

    luaut_push_udata_unit( L, unit, 1 );
    return 1;
}


/* NOT EXPOSED */
static int
push_converter ( lua_State *L, cv_converter* converter ) {

    if ( !converter )
	return push_error_status( L, 1 );

    luaut_push_udata_converter( L, converter, 1 );
    return 1;
}


/* NOT EXPOSED */
static int
push_string( lua_State *L, const char* string ) {

    if ( string ) {
	lua_pushstring( L, string );
	return 1;
    }

    else {
	lua_pushnil( L );
	lua_pushinteger( L, ut_get_status() );
	return 2;
    }


}

/* ***********************************************************************

   Interface to the udunits2 API

   *********************************************************************** */


/* --------------------------------------------------
   ut_system *ut_read_xml(const char *path)
   -------------------------------------------------- */

static int
read_xml( lua_State *L ) {

    const char* path = luaL_optstring( L, 1, NULL );

    set_lua_State( L );

    return push_system( L, ut_read_xml( path ) );
}

/* --------------------------------------------------
   ut_system *ut_new_system(void)
   -------------------------------------------------- */

static int
new_system( lua_State *L ) {

    set_lua_State( L );

    return push_system( L, ut_new_system() );
}


/* --------------------------------------------------

   void ut_free_system(ut_system*  system)

   not required

   -------------------------------------------------- */

/* ----------------------------------------------------
   ut_system *ut_get_system(const ut_unit * const unit)
   ---------------------------------------------------- */

static int
get_system( lua_State *L ) {

    ut_unit* unit = luaut_check_unit( L, 1 );

    set_lua_State( L );

    return push_system( L, ut_get_system( unit ) );
}


static int
system_dump( lua_State *L ) {

    ut_system* system = luaut_check_system( L, 1 );

    return 0;
}


/* ----------------------------------------------------------------------
   ut_unit *ut_get_dimensionless_unit_one(const ut_system * const system)
   ---------------------------------------------------------------------- */

static int
get_dimensionless_unit_one( lua_State *L ) {

    ut_system* system = luaut_check_system( L, 1 );

    set_lua_State( L );

    return push_unit( L, ut_get_dimensionless_unit_one( system ), 1 );
}


/* ------------------------------------------------------------------------------------
   ut_unit *ut_get_unit_by_name(const ut_system * const system, const char *const name)
   ------------------------------------------------------------------------------------ */
static int
get_unit_by_name( lua_State *L ) {

    ut_system* system = luaut_check_system( L, 1 );
    const char* name  = luaL_checkstring( L, 2 );

    set_lua_State( L );

    return push_unit( L, ut_get_unit_by_name( system, name ), 0 );
}



/* ----------------------------------------------------------------------------------------
   ut_unit *ut_get_unit_by_symbol(const ut_system * const system, const char *const symbol)
   ---------------------------------------------------------------------------------------- */
static int
get_unit_by_symbol( lua_State *L ) {

    ut_system* system = luaut_check_system( L, 1 );
    const char* symbol  = luaL_checkstring( L, 2 );

    set_lua_State( L );

    return push_unit( L, ut_get_unit_by_symbol( system, symbol ), 0 );
}

/* -----------------------------------------------------
   ut_status ut_set_second(const ut_unit * const second)
   ----------------------------------------------------- */
static int
set_second( lua_State *L ) {

    ut_unit* unit = luaut_check_unit( L, 1 );

    set_lua_State( L );

    return  push_status( L, ut_set_second( unit ) );
}


/* --------------------------------------------------------------------------------------------------
   ut_status ut_add_name_prefix(ut_system * const system, const char *const name, const double value)
   -------------------------------------------------------------------------------------------------- */
static int
add_name_prefix( lua_State *L ) {

    ut_system* system = luaut_check_system( L, 1 );

    const char* name = luaL_checkstring( L, 2 );

    const double value = luaL_checknumber( L, 3 );

    set_lua_State( L );

    return push_status( L, ut_add_name_prefix( system, name, value ) );
}

/* ------------------------------------------------------------------------------------------------------
   ut_status ut_add_symbol_prefix(ut_system * const system, const char *const symbol, const double value)
   ------------------------------------------------------------------------------------------------------ */
static int
add_symbol_prefix( lua_State *L ) {
    ut_system* system = luaut_check_system( L, 1 );

    const char* symbol = luaL_checkstring( L, 2 );

    const double value = luaL_checknumber( L, 3 );

    set_lua_State( L );

    return  push_status( L, ut_add_symbol_prefix( system, symbol, value ) );
}


/* ---------------------------------------------------
   ut_unit *ut_new_base_unit(ut_system * const system)
   --------------------------------------------------- */
static int
new_base_unit( lua_State *L ) {
    ut_system* system = luaut_check_system( L, 1 );

    set_lua_State( L );

    return  push_unit( L, ut_new_base_unit( system ), 1 );
}


/* ------------------------------------------------------------
   ut_unit *ut_new_dimensionless_unit(ut_system * const system)
   ------------------------------------------------------------ */
static int
new_dimensionless_unit( lua_State *L ) {
    ut_system* system = luaut_check_system( L, 1 );

    set_lua_State( L );

    return  push_unit( L, ut_new_dimensionless_unit( system ), 1 );
}


/* ---------------------------------------
   ut_unit *ut_clone(const ut_unit * unit)
   --------------------------------------- */
static int
clone( lua_State *L ) {

    ut_unit* unit = luaut_check_unit( L, 1 );

    set_lua_State( L );

    return  push_unit( L, ut_clone( unit ), 1 );

}

/* --------------------------------------
   void ut_free(ut_unit* const      unit)

   not required

   -------------------------------------- */


/* -------------------------------------------------------------------------------
   const char *ut_get_name(const ut_unit * const unit, const ut_encoding encoding)
   ------------------------------------------------------------------------------- */
static int
get_name( lua_State *L ) {

    ut_unit* unit = luaut_check_unit( L, 1 );

    ut_encoding encoding = check_encoding( L, 2 );

    set_lua_State( L );

    return  push_string( L, ut_get_name( unit, encoding ) );
}

/* ---------------------------------------------------------------------------------
   ut_status ut_map_name_to_unit(const char *const name, const ut_encoding encoding,
                                 const ut_unit * const unit)

   NOTE CHANGE IN ORDER OF ARGUMENTS
   --------------------------------------------------------------------------------- */
static int
map_name_to_unit( lua_State *L ) {
    ut_unit* unit = luaut_check_unit( L, 1 );

    const char *const name = luaL_checkstring( L, 2 );
    const ut_encoding encoding = check_encoding( L, 3 );

    set_lua_State( L );

    return  push_status( L, ut_map_name_to_unit( name, encoding, unit ) );
}



/* ---------------------------------------------------------------------------
   ut_status ut_unmap_name_to_unit(ut_system * system, const char *const name,
                                   const ut_encoding encoding)
   --------------------------------------------------------------------------- */
static int
unmap_name_to_unit( lua_State *L ) {

    ut_system* system = luaut_check_system( L, 1 );
    const char *const name = luaL_checkstring( L, 2 );
    const ut_encoding encoding = check_encoding( L, 3 );

    set_lua_State( L );

    return  push_status( L, ut_unmap_name_to_unit( system, name, encoding ) );
}

/* ---------------------------------------------------------------------------------
   ut_status ut_map_unit_to_name(const ut_unit * const unit, const char *const name,
                                 ut_encoding encoding)
   --------------------------------------------------------------------------------- */

static int
map_unit_to_name( lua_State *L ) {
    ut_unit* unit = luaut_check_unit( L, 1 );

    const char *const name = luaL_checkstring( L, 2 );

    const ut_encoding encoding = check_encoding( L, 3 );

    set_lua_State( L );

    return  push_status( L, ut_map_unit_to_name( unit, name, encoding ) );
}

/* ---------------------------------------------------------------------------------
   ut_status ut_unmap_unit_to_name(const ut_unit * const unit, ut_encoding encoding)
 --------------------------------------------------------------------------------- */
static int
unmap_unit_to_name( lua_State *L ) {

    ut_unit* unit = luaut_check_unit( L, 1 );

    const ut_encoding encoding = check_encoding( L, 2 );

    set_lua_State( L );

    return  push_status( L, ut_unmap_unit_to_name( unit, encoding ) );
}

/* ---------------------------------------------------------------------------------
   const char *ut_get_symbol(const ut_unit * const unit, const ut_encoding encoding)
   --------------------------------------------------------------------------------- */
static int
get_symbol( lua_State *L ) {

    ut_unit* unit = luaut_check_unit( L, 1 );

    ut_encoding encoding = check_encoding( L, 2 );

    set_lua_State( L );

    return  push_string( L, ut_get_symbol( unit, encoding ) );
}

/* -------------------------------------------------------------------------------------
   ut_status ut_map_symbol_to_unit(const char *const symbol, const ut_encoding encoding,
                                   const ut_unit * const unit)
   NOTE CHANGE IN ORDER OF ARGUMENTS
   ------------------------------------------------------------------------------------- */
static int
map_symbol_to_unit( lua_State *L ) {

    ut_unit* unit = luaut_check_unit( L, 1 );

    const char *const symbol = luaL_checkstring( L, 2 );

    ut_encoding encoding = check_encoding( L, 3 );

    set_lua_State( L );

    return  push_status( L, ut_map_symbol_to_unit( symbol, encoding, unit ) );
}



/* -------------------------------------------------------------------------------
   ut_status ut_unmap_symbol_to_unit(ut_system * system, const char *const symbol,
                                     const ut_encoding encoding)
   ------------------------------------------------------------------------------- */
static int
unmap_symbol_to_unit( lua_State *L ) {

    ut_system* system = luaut_check_system( L, 1 );

    const char *const symbol = luaL_checkstring( L, 2 );
    const ut_encoding encoding = check_encoding( L, 3 );

    set_lua_State( L );

    return  push_status( L, ut_unmap_symbol_to_unit( system, symbol, encoding ) );
}

/* ------------------------------------------------------------------------------
  ut_status ut_map_unit_to_symbol(const ut_unit * unit, const char *const symbol,
                                  ut_encoding encoding)
   ------------------------------------------------------------------------------ */
static int
map_unit_to_symbol( lua_State *L ) {
    ut_unit* unit = luaut_check_unit( L, 1 );

    const char *const symbol = luaL_checkstring( L, 2 );

    ut_encoding encoding = check_encoding( L, 3 );

    set_lua_State( L );

    return  push_status( L, ut_map_unit_to_symbol( unit, symbol, encoding ) );
}

/* -----------------------------------------------------------------------------------
   ut_status ut_unmap_unit_to_symbol(const ut_unit * const unit, ut_encoding encoding)
   ----------------------------------------------------------------------------------- */
static int
unmap_unit_to_symbol( lua_State *L ) {

    ut_unit* unit = luaut_check_unit( L, 1 );

    ut_encoding encoding = check_encoding( L, 2 );

    set_lua_State( L );

    return  push_status( L, ut_unmap_unit_to_symbol( unit, encoding ) );
}

/* ---------------------------------------------------
   int ut_is_dimensionless(const ut_unit * const unit)
   --------------------------------------------------- */
static int
is_dimensionless( lua_State *L ) {
    ut_unit* unit = luaut_check_unit( L, 1 );

    set_lua_State( L );

    lua_pushboolean( L, ut_is_dimensionless( unit ) );
    return 1;
}

/* ----------------------------------------------------------------------------
   int ut_same_system(const ut_unit * const unit1, const ut_unit * const unit2)
   ---------------------------------------------------------------------------- */
static int
same_system( lua_State *L ) {
    ut_unit* unit1 = luaut_check_unit( L, 1 );
    ut_unit* unit2 = luaut_check_unit( L, 2 );

    set_lua_State( L );

    lua_pushboolean( L, ut_same_system(unit1, unit2 ) );
    return 1;
}

/* ------------------------------------------------------------------------
   int ut_compare(const ut_unit * const unit1, const ut_unit * const unit2)
   ------------------------------------------------------------------------ */
static int
compare( lua_State *L ) {
    ut_unit* unit1 = luaut_check_unit( L, 1 );
    ut_unit* unit2 = luaut_check_unit( L, 2 );

    set_lua_State( L );

    lua_pushinteger( L, ut_compare( unit1, unit2 ) );
    return 1;
}


/* --------------------------------------------------------------------------------
   int ut_are_convertible(const ut_unit * const unit1, const ut_unit * const unit2)
   -------------------------------------------------------------------------------- */
static int
are_convertible( lua_State *L ) {
    ut_unit* unit1 = luaut_check_unit( L, 1 );
    ut_unit* unit2 = luaut_check_unit( L, 2 );

    set_lua_State( L );

    lua_pushboolean( L, ut_are_convertible( unit1, unit2 ) );
    return 1;
}

/* ------------------------------------------------------------------------
   cv_converter *ut_get_converter(ut_unit * const from, ut_unit * const to)
   ------------------------------------------------------------------------ */
static int
get_converter( lua_State *L ) {
    ut_unit* unit1 = luaut_check_unit( L, 1 );
    ut_unit* unit2 = luaut_check_unit( L, 2 );

    set_lua_State( L );

    return push_converter( L, ut_get_converter( unit1, unit2 ) );
}

/* ------------------------------------------------------------------
   ut_unit *ut_scale(const double factor, const ut_unit * const unit)
   NOTE CHANGE IN ORDER OF ARGUMENTS
*/
static int
scale( lua_State *L ) {
    ut_unit* unit = luaut_check_unit( L, 1 );

    double factor = luaL_checknumber( L, 2 );

    set_lua_State( L );

    return push_unit( L, ut_scale( factor, unit), 1 );
}


/* -------------------------------------------------------------------
   ut_unit *ut_offset(const ut_unit * const unit, const double offset)
   ------------------------------------------------------------------- */
static int
offset( lua_State *L ) {
    ut_unit* unit = luaut_check_unit( L, 1 );

    double offset = luaL_checknumber( L, 2 );

    set_lua_State( L );

    return push_unit( L, ut_offset( unit, offset ), 0 );
}


/* ---------------------------------------------------------------------------
   ut_unit *ut_offset_by_time(const ut_unit * const unit, const double origin)
   --------------------------------------------------------------------------- */
static int
offset_by_time( lua_State *L ) {
    ut_unit* unit = luaut_check_unit( L, 1 );

    double origin = luaL_checknumber( L, 2 );

    set_lua_State( L );

    return  push_unit( L, ut_offset_by_time( unit, origin ), 0 );
}


/* ------------------------------------------------------------------------------
   ut_unit *ut_multiply(const ut_unit * const unit1, const ut_unit * const unit2)
   ------------------------------------------------------------------------------ */
static int
multiply( lua_State *L ) {

    ut_unit* unit1 = luaut_check_unit( L, 1 );
    ut_unit* unit2 = luaut_check_unit( L, 2 );

    set_lua_State( L );

    return  push_unit( L, ut_multiply( unit1, unit2 ), 0 );
}

/* ----------------------------------------------
   ut_unit *ut_invert(const ut_unit * const unit)
   ---------------------------------------------- */
static int
invert( lua_State *L ) {
    ut_unit* unit = luaut_check_unit( L, 1 );

    set_lua_State( L );

    return  push_unit( L, ut_invert( unit ), 1 );
}


/* -----------------------------------------------------------------------------
   ut_unit *ut_divide(const ut_unit * const number, const ut_unit * const denom)
   ----------------------------------------------------------------------------- */
static int
divide( lua_State *L ) {

    ut_unit* unit1 = luaut_check_unit( L, 1 );
    ut_unit* unit2 = luaut_check_unit( L, 2 );

    set_lua_State( L );

    return  push_unit( L, ut_divide( unit1, unit2 ), 0 );
}


/* --------------------------------------------------------------
   ut_unit *ut_raise(const ut_unit * const unit, const int power)
   --------------------------------------------------------------*/
static int
raise( lua_State *L ) {

    ut_unit* unit = luaut_check_unit( L, 1 );
    int power = luaL_checkinteger( L, 2 );

    set_lua_State( L );

    return  push_unit( L, ut_raise( unit, power ), 1 );
}


/* ------------------------------------------------------------
   ut_unit *ut_root(const ut_unit * const unit, const int root)
   ------------------------------------------------------------ */
static int
root( lua_State *L ) {
    ut_unit* unit = luaut_check_unit( L, 1 );

    int root = luaL_checkinteger( L, 2 );

    set_lua_State( L );

    return push_unit( L, ut_root( unit, root ), 1 );
}


/* -------------------------------------------------------------------
   ut_unit *ut_log(const double base, const ut_unit * const reference)
   NOTE CHANGE IN ORDER OF ARGUMENTS
   ------------------------------------------------------------------- */
static int
unit_log( lua_State *L ) {

    ut_unit* unit = luaut_check_unit( L, 1 );
    double base   = luaL_checknumber( L, 2 );

    set_lua_State( L );

    return push_unit( L, ut_log( base, unit ), 1 );
}


/* ---------------------------------------------------------------------------
   ut_unit *ut_parse(const ut_system * const system, const char *const string,
                     const ut_encoding encoding)
   --------------------------------------------------------------------------- */
static int
parse( lua_State *L ) {

    ut_system* system = luaut_check_system( L, 1 );
    const char *const string = luaL_checkstring( L, 2 );
    const ut_encoding encoding = check_optencoding( L, 3, UT_ASCII );

    set_lua_State( L );

    return push_unit( L, ut_parse( system, string, encoding ), 0 );
}

/* -------------------------------------------------------------
   char *ut_trim(char *const string, const ut_encoding encoding)
 ------------------------------------------------------------- */
static int
trim( lua_State *L ) {

    const char* string = luaL_checkstring(L, 1);
    const ut_encoding encoding = check_encoding( L, 2 );

    char *copy = malloc( strlen( string ) + 1 );

    set_lua_State( L );

    if ( copy == NULL ) {
	ut_set_status( UT_OS );
	return push_error_status( L, 1 );
    }

    /* bug in udunits-2.1.23; ut_trim returns the wrong pointer,
       it's supposed to return the pointer it was passed
     */
    strcpy( copy, string );
    ut_trim( copy, encoding );
    lua_pushstring( L, copy );
    free( copy );

    return 1;
}


/* --------------------------------------------------------------------------------
   int ut_format(const ut_unit * const unit, char *buf, size_t size, unsigned opts)

   NOTE CHANGE IN ARGUMENTS
   -------------------------------------------------------------------------------- */
static int
format( lua_State *L ) {
    char buf[1024];
    ut_unit* unit = luaut_check_unit( L, 1 );
    unsigned opts = luaL_checkinteger( L, 2 );

    int size;

    set_lua_State( L );

    size = ut_format( unit, buf, sizeof( buf ), opts );

    if ( size == -1 )
	return push_error_status( L, 1 );

    else {

	lua_pushlstring( L, buf, size );
	return 1;
    }
}


/* ------------------------------------------------------------------------------
   ut_status ut_accept_visitor(const ut_unit * const unit,
                               const ut_visitor * const visitor, void *const arg)

   NOT IMPLEMENTED

   ------------------------------------------------------------------------------ */


/* ---------------------------------------------------
   double ut_encode_date(int year, int month, int day)
   --------------------------------------------------- */
static int
encode_date( lua_State *L ) {

    int year  = luaL_checkinteger( L, 1 );
    int month = luaL_checkinteger( L, 2 );
    int day   = luaL_checkinteger( L, 3 );

    set_lua_State( L );

    lua_pushnumber( L, ut_encode_date( year, month, day ) );
    return 1;
}

/* --------------------------------------------------------------
   double ut_encode_clock(int hours, int minutes, double seconds)
   -------------------------------------------------------------- */
static int
encode_clock( lua_State *L ) {

    int hours  = luaL_checkinteger( L, 1 );
    int minutes = luaL_checkinteger( L, 2 );
    int seconds   = luaL_checkinteger( L, 3 );

    set_lua_State( L );

    lua_pushnumber( L, ut_encode_clock( hours, minutes, seconds ) );
    return 1;
}


/* ----------------------------------------------------------------------------
   double ut_encode_time(const int year, const int month, const int day,
                         const int hour, const int minute, const double second)
   ---------------------------------------------------------------------------- */
static int
encode_time( lua_State *L ) {
    int year   = luaL_checkinteger( L, 1 );
    int month  = luaL_checkinteger( L, 2 );
    int day    = luaL_checkinteger( L, 3 );
    int hour   = luaL_checkinteger( L, 4 );
    int minute = luaL_checkinteger( L, 5 );
    double second = luaL_checknumber( L, 6 );

    set_lua_State( L );

    lua_pushnumber( L, ut_encode_time(year, month, day, hour, minute, second) );
    return 1;
}

/* -------------------------------------------------------------------------------
   void ut_decode_time(double value, int *year, int *month, int *day,
                       int *hour, int *minute, double *second, double *resolution)
   ------------------------------------------------------------------------------- */
static int
decode_time( lua_State *L ) {

    double value = luaL_checknumber( L, 1 );

    int year, month, day, hour, minute;
    double second, resolution;

    set_lua_State( L );

    ut_decode_time( value, &year, &month, &day, &hour, &minute, &second, &resolution);

    lua_pushinteger( L, year );
    lua_pushinteger( L, month );
    lua_pushinteger( L, day );
    lua_pushinteger( L, hour );
    lua_pushinteger( L, minute );
    lua_pushnumber( L, second );
    lua_pushnumber( L, resolution );

    return 7;
}

/* -----------------------------
   ut_status ut_get_status(void)
   ----------------------------- */
static int
get_status( lua_State *L ) {

    lua_pushinteger( L, ut_get_status());
    return 1;
}

/* ------------------------------------
   void ut_set_status(ut_status status)
   ------------------------------------ */
static int
set_status( lua_State *L ) {

    set_lua_State( L );

    ut_set_status( luaL_checkinteger( L, 1 ) );
    return 0;
}

/* -------------------------------------------------------
   int ut_handle_error_message(const char *const fmt, ...)

   NOTE CHANGE IN ARGUMENTS

   ------------------------------------------------------- */
static int
handle_error_message( lua_State *L ) {

    int size;
    const char *string = luaL_checkstring( L, 1 );

    set_lua_State( L );

    size = ut_handle_error_message( "%s", string );
    lua_pushinteger( L, size );
    return 1;
}


/* -----------------------------------------------------------------------

   ut_error_message_handler
        ut_set_error_message_handler(ut_error_message_handler handler)

   Accepts strings:
   * 'stderr' -- output to stderr
   * 'ignore' -- output to /dev/null
   * Lua function
   * Lua message_handler object (encapsulated unknown C message handler)

   Returns last handler set

   ----------------------------------------------------------------------- */
static int
set_error_message_handler( lua_State *L ) {

    ut_error_message_handler last;
    ut_error_message_handler next;

    int arg = lua_gettop( L );

    /* gotta have at least one argument */
    luaL_checkany( L, 1 );

    set_lua_State( L );

    switch ( lua_type( L, arg ) ) {

    case LUA_TSTRING:
	{
	    const char* str = lua_tostring( L, 1 );

	    if ( ! strcmp( str, "stderr" ) ) {

		next = ut_write_to_stderr;

	    }
	    else if ( ! strcmp( str, "ignore" ) ) {

		next = ut_ignore;

	    }

	    else {

		lua_pushnil( L );
		lua_pushfstring( L, "unknown standard message handler: %s\n", str );
		return 2;
	    }
	}

	break;

    case LUA_TFUNCTION:

	next = lua_message_handler;
	break;

    case LUA_TUSERDATA:
	next = luaut_check_message_handler(L, 1 );
	break;

    default:

	return luaL_argerror( L, 1, "expected strings 'stderr', 'ignore' or function or C message_handler" );
	break;

    };

    last = ut_set_error_message_handler( next );

    if ( last == lua_message_handler ) {

	/* retrieve last registered value */
	retrieve_message_handler( L );

	if ( lua_isnil( L, -1 ) ) {
	    lua_pushstring( L, "internal error: lua_message_handler set, but no Lua handler?\n" );
	    lua_error( L );
	}

    }

    else if ( last == ut_write_to_stderr )
	lua_pushstring( L, "stderr" );

    else if ( last == ut_ignore )
	lua_pushstring( L, "ignore" );

    else
	luaut_push_udata_message_handler(L, last, 1 );

    /* set or reset Lua message handler function.  must do this after
       retrieving the last Lua message handler function or it would have
       been lost */
    if ( next == lua_message_handler ) lua_pushvalue( L, arg );
    else                               lua_pushnil( L );

    store_message_handler( L );

    return 1;
}


/* -----------------------------------------------------------
   int ut_write_to_stderr(const char *const fmt, va_list args)

   NOT REQUIRED

   ----------------------------------------------------------- */

/* --------------------------------------------------
   int ut_ignore(const char *const fmt, va_list args)

   NOT REQUIRED

   -------------------------------------------------- */


/* ********************************************************************************
			  Converter Routines
*/

/* ---------------------------------
   cv_converter*cv_get_trivial(void)
 --------------------------------- */

static int
get_trivial( lua_State *L ) {

    set_lua_State( L );

    return push_converter( L, cv_get_trivial() );

}

/* ---------------------------------
   cv_converter*cv_get_inverse(void)
 --------------------------------- */

static int
get_inverse( lua_State *L ) {

    set_lua_State( L );

    return push_converter( L, cv_get_inverse() );

}



/* ----------------------------------------------
   cv_converter* cv_get_scale(const double slope)
   ---------------------------------------------- */
static int
get_scale( lua_State *L ) {

    double slope = luaL_checknumber( L, 1 );

    set_lua_State( L );

    return push_converter( L, cv_get_scale( slope ) );

}


/* ---------------------------------------------------
   cv_converter* cv_get_offset(const double intercept)
   --------------------------------------------------- */
static int
get_offset( lua_State *L ) {

    double intercept = luaL_checknumber( L, 1 );

    set_lua_State( L );

    return push_converter( L, cv_get_offset( intercept ) );

}

/* --------------------------------------------------------------------------
   cv_converter* cv_get_galilean( const double slope, const double intercept)
   -------------------------------------------------------------------------- */

static int
get_galilean( lua_State *L ) {

    double slope     = luaL_checknumber( L, 1 );
    double intercept = luaL_checknumber( L, 2 );

    set_lua_State( L );

    return push_converter( L, cv_get_galilean( slope, intercept ) );
}

/* -------------------------------------------
   cv_converter* cv_get_log(const double base)
   ------------------------------------------- */

static int
get_log( lua_State *L ) {

    double base = luaL_checknumber( L, 1 );

    set_lua_State( L );

    return push_converter( L, cv_get_log( base ) );
}

/* -------------------------------------------
   cv_converter* cv_get_pow(const double base)
   ------------------------------------------- */

static int
get_pow( lua_State *L ) {

    double base = luaL_checknumber( L, 1 );

    set_lua_State( L );

    return push_converter( L, cv_get_pow( base ) );
}

/* --------------------------------------------------------------------------------
   cv_converter* cv_combine( cv_converter* const first, cv_converter* const second)
   -------------------------------------------------------------------------------- */

static int
combine( lua_State *L ) {

    cv_converter* const first = luaut_check_converter( L, 1 );
    cv_converter* const second = luaut_check_converter( L, 2 );

    set_lua_State( L );

    return push_converter( L, cv_combine( first, second ) );
}

/* --------------------------------------
   void cv_free(cv_converter* const conv)

   NOT REQUIRED
   -------------------------------------- */

/* -------------------------------------------------------------------------
  float cv_convert_float( const cv_converter* converter, const float value)
  double cv_convert_double(const cv_converter* converter, const double value)

  COMBINED
 ------------------------------------------------------------------------- */

static int
convert( lua_State *L ) {

    const cv_converter* const converter = luaut_check_converter( L, 1 );
    double value = luaL_checknumber( L, 2 );

    set_lua_State( L );

    lua_pushnumber( L, cv_convert_double( converter, value ) );
    return 1;
}

/* ----------------------------------------------------------------------------------
   float* cv_convert_floats(const cv_converter* converter, const float* const in,
                            const size_t count, float* out);

   double* cv_convert_doubles( const cv_converter* converter, const double* const in,
                               const size_t count, double* out);

   NOT IMPLEMENTED

   ---------------------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   int cv_get_expression( const cv_converter* const conv, char* const buf,
                          size_t max, const char* const variable )

   NOTE CHANGE IN ARGUMENTS

   ----------------------------------------------------------------------- */
static int
get_expression( lua_State *L ) {
    char buf[1024];
    const cv_converter* const converter = luaut_check_converter( L, 1 );
    const char * variable = luaL_checkstring( L, 2 );

    int size;

    set_lua_State( L );

    size = cv_get_expression( converter, buf, sizeof(buf), variable );

    if ( size < 0 ) {
	lua_pushnil( L );
	return 1;
    }

    else {
	lua_pushlstring( L, buf, size >= sizeof(buf) ? sizeof(buf) - 1 : size  );
	return 1;
    }

}


/* -------------------------------------------------------------------------

   Exposed support routines

 */


static int
isa( lua_State *L ) {

    const char* type = luaL_checkstring(L, 2 );
    const char* mt_name = NULL;

    mt_name = strcmp( type, "system" ) == 0                ? luaut_mt_system
	    : strcmp( type, "unit" ) == 0                  ? luaut_mt_unit
	    : strcmp( type, "converter" ) == 0             ? luaut_mt_converter
            : strcmp( type, "error_message_handler" ) == 0 ? luaut_mt_error_message_handler
	                                                   : NULL;


    if ( NULL == mt_name )
	return luaL_error( L, "requested object type (%s) is not a known udunits-2 object type", type );

    lua_pushboolean( L, luaut_is_udata_type( L, -2, mt_name ) );
    return 1;
}


/* ----------------------------------------------------------------------------------

   More support routines

 */

static void
register_functions( lua_State* L, luaL_Reg* list ) {

    /* populate the table */
    for( ; list->name ; list++ ) {
	lua_pushcfunction(L, list->func );
	lua_setfield( L, -2, list->name );
    }

}

typedef struct {
    const char* name;
    int value;
} EnumList;

#define ENUM(name) {#name, UT_##name}

static void
register_enums( lua_State* L, EnumList* list ) {

    /* populate the table */
    for( ; list->name ; list++ ) {
	lua_pushinteger(L, list->value );
	lua_setfield( L, -2, list->name );
    }

}

static EnumList
encoding[] = {
    ENUM( ASCII ),
    ENUM( ISO_8859_1 ),
    ENUM( LATIN1 ),
    ENUM( UTF8 ),
    { NULL, 0 }
};

static EnumList
status[] = {
    ENUM( SUCCESS ),
    ENUM( BAD_ARG ),
    ENUM( EXISTS ),
    ENUM( NO_UNIT ),
    ENUM( OS ),
    ENUM( NOT_SAME_SYSTEM ),
    ENUM( MEANINGLESS ),
    ENUM( NO_SECOND ),
    ENUM( VISIT_ERROR ),
    ENUM( CANT_FORMAT ),
    ENUM( SYNTAX ),
    ENUM( UNKNOWN ),
    ENUM( OPEN_ARG ),
    ENUM( OPEN_ENV ),
    ENUM( OPEN_DEFAULT ),
    ENUM( PARSE ),
    { NULL, 0 }
};

static EnumList
format_opts[] = {

    ENUM( NAMES ),
    ENUM( DEFINITION ),

};

#define add(name) {#name, name }

static luaL_Reg
 udunits2_functions[] = {

    add( trim ),
    add( encode_date ),
    add( encode_clock),
    add( encode_time ),
    add( decode_time ),
    add( get_status ),
    add( set_status ),
    add( set_error_message_handler ),
    add( handle_error_message ),
    add( isa ),
    { NULL, NULL }
};


static luaL_Reg udunits2_system_functions[] = {

    add( read_xml ),
    { "new", new_system },

    { NULL, NULL }
};

static luaL_Reg udunits2_system_methods[] = {

    add( get_dimensionless_unit_one ),
    add( get_unit_by_name ),
    add( get_unit_by_symbol ),
    add( add_name_prefix ),
    add( add_symbol_prefix ),
    add( new_base_unit ),
    add( new_dimensionless_unit ),
    add( unmap_name_to_unit ),
    add( unmap_symbol_to_unit ),
    add( parse ),
    { "dump", system_dump},


    { NULL, NULL }
};

static luaL_Reg udunits2_unit_methods[] = {

    add( get_system ),
    add( set_second ),
    add( clone ),
    add( get_name ),
    add( map_name_to_unit ),
    add( map_unit_to_name ),
    add( unmap_unit_to_name ),
    add( get_symbol ),
    add( map_symbol_to_unit ),
    add( map_unit_to_symbol ),
    add( unmap_unit_to_symbol ),
    add( is_dimensionless ),
    add( same_system ),
    add( compare ),
    add( are_convertible ),
    add( get_converter ),
    add( scale ),
    add( offset ),
    add( offset_by_time ),
    add( multiply ),
    add( invert ),
    add( divide ),
    add( raise ),
    add( root ),
    { "log", unit_log },
    add( format ),
    { NULL, NULL }
};

static luaL_Reg udunits2_unit_functions[] = {

    add( same_system ),
    add( compare ),
    add( are_convertible ),
    add( get_converter ),
    add( multiply ),
    add( divide ),
    { NULL, NULL }
};


static luaL_Reg
 udunits2_converter_functions[] = {

    add( combine ),
    add( get_trivial ),
    add( get_inverse ),
    add( get_scale ),
    add( get_offset ),
    add( get_galilean ),
    add( get_converter ),
    add( get_log ),
    add( get_pow ),

};

static luaL_Reg
 udunits2_converter_methods[] = {

    add( combine ),
    add( convert ),
    add( get_expression ),

};


/*

  objects may be created indirectly via other classes, e.g.

   - units are created solely from methods in the system class

   - converters may be created from the units class

   so, make sure that the class' metatables are always around

*/

static void
init( lua_State * L ) {

    int top = lua_gettop( L );

    /* our own stash in the registry */
    lua_getfield( L, LUA_REGISTRYINDEX, "udunits2" );
    if ( lua_isnil( L, -1 ) ) {

	/* create table and store it in the registry*/
	lua_newtable( L );
	lua_setfield( L, LUA_REGISTRYINDEX, "udunits2" );
    }

    /* udunits.system */
    if ( luaL_newmetatable( L, luaut_mt_system ) ) {

	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index" );

	lua_pushcfunction(L, system_gc);
	lua_setfield(L, -2, "__gc" );

	/* add methods to it */
	luaL_register( L, NULL, udunits2_system_methods);
    }

    /* udunits.units */
    if ( luaL_newmetatable( L, luaut_mt_unit ) ) {
	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index" );

	lua_pushcfunction(L, unit_gc);
	lua_setfield(L, -2, "__gc" );

	/* add methods to it */
	register_functions( L, udunits2_unit_methods);
    }

    /* udunits.converter */
    if ( luaL_newmetatable( L, luaut_mt_converter ) ) {

	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index" );

	lua_pushcfunction(L, converter_gc);
	lua_setfield(L, -2, "__gc" );

	/* add methods to it */
	register_functions( L, udunits2_converter_methods);
    }

    /* udunits.error_message_handler */
    luaL_newmetatable( L, luaut_mt_error_message_handler );

    lua_settop( L, top );
}


int luaopen_udunits2( lua_State *L ) {

    init( L );

    lua_newtable( L );
    register_enums( L, encoding );
    register_enums( L, status );
    register_enums( L, format_opts );
    register_functions( L, udunits2_functions );

    return 1;
}

int luaopen_udunits2_system( lua_State *L ) {

    init( L );

    lua_newtable( L );
    register_functions( L, udunits2_system_functions );

    return 1;
}

int luaopen_udunits2_unit( lua_State *L ) {

    init( L );

    lua_newtable( L );
    register_functions( L, udunits2_unit_functions );

    return 1;
}

int luaopen_udunits2_converter( lua_State *L ) {

    init( L );

    lua_newtable( L );
    register_functions( L, udunits2_converter_functions );

    return 1;
}

