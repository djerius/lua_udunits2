/* --8<--8<--8<--8<--
 *
 * Copyright (C) 2011 Smithsonian Astrophysical Observatory
 *
 * This file is part of lua_udunits2
 *
 * lua_udunits2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -->8-->8-->8-->8-- */


#include "lua_udunits2.h"

/* based upon luaL_checkudata, but doesn't throw error */
int
luaut_is_udata_type( lua_State *L, int idx, const char *tname ) {

    void *p = lua_touserdata( L, idx );

    if ( p && lua_getmetatable( L, idx ) ) {

	lua_getfield( L, LUA_REGISTRYINDEX, tname );
	if ( lua_rawequal( L, -1, -2 ) ) {
	    lua_pop( L, 2 );
	    return 1;
	}

    }

    return 0;
}
