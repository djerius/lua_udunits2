/* --8<--8<--8<--8<--
 *
 * Copyright (C) 2011 Smithsonian Astrophysical Observatory
 *
 * This file is part of lua-udunits
 *
 * lua-udunits is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -->8-->8-->8-->8-- */

#ifndef LUA_UDUNITS_H
#define LUA_UDUNITS_H

#include <lua.h>
#include <lauxlib.h>

#include <udunits2.h>
#include <converter.h>

#define luaut_mt_system    "udunits.system"
#define luaut_mt_unit      "udunits.unit"
#define luaut_mt_converter "udunits.converter"
#define luaut_mt_error_message_handler "udunits.error_message_handler"


typedef struct {

    union {

	ut_system*               system;
	ut_unit*                 unit;
	cv_converter*            converter;
	ut_error_message_handler error_message_handler;
    } p;

    int manage;

} LuaUTContainer;

/* push a Lua full userdata on the stack containing a pointer to something */
#define luaut_push_udata_type(L,data,elem,is_lua_managed)	\
    do {\
    LuaUTContainer* tmp = (LuaUTContainer*) lua_newuserdata(L, sizeof(LuaUTContainer));\
    tmp->p.elem = data;\
    tmp->manage = is_lua_managed;\
    luaL_getmetatable( L, luaut_mt_##elem ); \
    if ( lua_isnil( L, -1 ) ) \
	luaL_error( L, "internal error; can't find metatable for %s\n", luaut_mt_##elem ); \
    lua_setmetatable(L, -2 ); \
    } while(0)

#define luaut_push_udata_system(L,data,is_lua_managed)		\
    luaut_push_udata_type(L,data,system,is_lua_managed)

#define luaut_push_udata_unit(L,data,is_lua_managed)		\
    luaut_push_udata_type(L,data,unit,is_lua_managed)

#define luaut_push_udata_converter(L,data,is_lua_managed)		\
    luaut_push_udata_type(L,data,converter,is_lua_managed)

#define luaut_push_udata_message_handler(L,data,is_lua_managed)		\
    luaut_push_udata_type(L,data,error_message_handler,is_lua_managed)

/* extract a udunits object pointer from the given Lua full userdata
   block */

#define luaut_udata2type(udata,elem)       (((LuaUTContainer*)udata)->p.elem)
#define luaut_udata2system(udata)          luaut_udata2type(udata,system)
#define luaut_udata2unit(udata)            luaut_udata2type(udata,unit)
#define luaut_udata2converter(udata)       luaut_udata2type(udata,converter)
#define luaut_udata2message_handler(udata) luaut_udata2type(udata,error_message_handler)

/* extract a udunits object pointer from the given element on the Lua stack */
#define luaut_totype(L,idx,elem) (((LuaUTContainer*)(lua_touserdata(L,idx)))->p.elem)
#define luaut_tosystem(L,idx)          luaut_totype(L,idx,system)
#define luaut_tounit(L,idx)            luaut_totype(L,idx,unit)
#define luaut_toconverter(L,idx)       luaut_totype(L,idx,converter)
#define luaut_tomessage_handler(L,idx) luaut_totype(L,idx,error_message_handler)
#define luaut_tocontainer(L,idx)       ((LuaUTContainer*)(lua_touserdata(L,idx)))

/* check that the specified argument is a userdata of the correct type
   and return the encapsulated udunits object pointer */

#define luaut_check_system(L, narg)						\
    luaut_udata2system(luaL_checkudata(L, narg, luaut_mt_system ))

#define luaut_check_unit(L, narg) \
    luaut_udata2unit(luaL_checkudata(L, narg, luaut_mt_unit ))

#define luaut_check_converter(L, narg) \
    luaut_udata2converter(luaL_checkudata(L, narg, luaut_mt_converter ))

#define luaut_check_message_handler(L, narg) \
    luaut_udata2message_handler(luaL_checkudata(L, narg, luaut_mt_error_message_handler ))


/* return true if the given acceptable index is a userdata of the
   specific type */


#define luaut_is_system(L, narg)						\
    luaut_is_udata_type(L, narg, luaut_mt_system )

#define luaut_is_unit(L, narg) \
    luaut_is_udata_type(L, narg, luaut_mt_unit )

#define luaut_is_converter(L, narg) \
    luaut_is_udata_type(L, narg, luaut_mt_converter )

#define luaut_is_message_handler(L, narg) \
    luaut_is_udata_type(L, narg, luaut_mt_error_message_handler )


#ifdef __cplusplus              /* needed for C++ code */
extern "C"
{
#endif

    int luaut_is_udata_type( lua_State *L, int idx, const char *tname );

#ifdef __cplusplus
}
#endif


#endif /* !  LUA_UDUNITS_H */

